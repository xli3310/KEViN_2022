import os
import json
from itertools import combinations

import pandas as pd
from tqdm import tqdm
import argparse
from pathlib import Path

from utils import train, test

tqdm.pandas()
random_state=102349

if __name__ == '__main__':

    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("train_file", type=Path, action="store",
                                 help="The file with the training data")
    argument_parser.add_argument("dev_file", type=Path, action="store",
                                 help="The file with the dev data")
    argument_parser.add_argument("--indep", action="store_true", default=False,
                                 help="Set this to train separate models for the validity and novelty tasks")
    args = argument_parser.parse_args()

    DATA_PATH = os.path.dirname(args.dev_file)

    df = pd.read_csv(args.train_file, index_col=0)
    df_dev = pd.read_csv(args.dev_file, index_col=0)

    ent_cols = df.columns[df.columns.str.contains('distrib')]

    df[ent_cols] = df[ent_cols].applymap(eval)
    df_dev[ent_cols] = df_dev[ent_cols].applymap(eval)

    features = [
           'EntailmentPtoC_distrib',
           'EntailmentCtoP_distrib',
           'SBERT_cosine_sim',
           'irrelevancy',
           'sumDis',
           'minDis',
           'maxDis',
           'aveDisA', 'aveDisANew', 'aveDisAmeer1', 'aveDisAmeer2']

    normalized_features = ['SBERT_cosine_sim',
           'irrelevancy',
           'sumDis',
           'minDis',
           'maxDis',
           'aveDisA', 'aveDisANew', 'aveDisAmeer1', 'aveDisAmeer2']

    if args.indep:
        targets = ['Validity', 'Novelty']
        out_file = 'grid_search_indep.csv'
        out_file_features = 'best_features_indep.json'
    else:
        targets = ['ValNov']
        out_file = 'grid_search_joint.csv'
        out_file_features = 'best_features_joint.json'

    best = {}
    results = []

    reg_values = [0.001, 0.01, 0.1]

    for target in targets:
        print(target)
        best[target] = {'score': 0, 'features': [], 'reg_value': 0}

        for r in range(1, 5):
            for f in combinations(features,r):
                f=list(f)
                for c in reg_values:
                    clf = train(train_df=df, features=f, target=target, reg_value=c, random_state=random_state)
                    score = test(test_df=df_dev, clf=clf, features=f, target=target)

                    if score['f1_macro'] > best[target]['score']:
                        best[target]['score'] = score['f1_macro']
                        best[target]['features'] = f
                        best[target]['reg_value'] = c

                    results.append({'target': target, 'score': score['f1_macro'], 'features': f, 'reg_value': c})

                    print('score:', score, '\tfeatures:', f, '\treg_value:', c)

        print('\n')

    for target in targets:
        print('Best features for', target, ':')
        print(best[target]['features'])
        print('...with reg_value of', best[target]['reg_value'])
        print('...and F-1 score %.3f' % best[target]['score'])

    with open(os.path.join(DATA_PATH, out_file_features), 'w') as file:
        json.dump(best, file)

    results = pd.DataFrame(results)
    results.to_csv(os.path.join(DATA_PATH, out_file))