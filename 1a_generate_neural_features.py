import os
import argparse
from pathlib import Path

import pandas as pd
from tqdm import tqdm
from numpy import dot
from numpy.linalg import norm
from transformers import AutoModelForSequenceClassification, AutoModel, AutoTokenizer
import torch

tqdm.pandas()
random_state=102349

# returns the possibility for the classes "Contradiction", "Neutral" and "Entailment"
def zsl_multi_classifier(row, sequence, context, distrib):
    x = tokenizer.encode(row[sequence], row[context], return_tensors='pt',
                         truncation_strategy='only_first').to(device)
    if distrib:
        results = torch.nn.functional.softmax(classifier(x)[0], -1).squeeze(0).cpu().detach().numpy()
    else:
        results = classifier(x)[0].argmax(-1).item()
    return results


def run(dataframe, out_file, out_column, sequence, context, distrib=False, reset=False):
    number_lines = len(dataframe)
    chunksize = 12

    if (out_file is None) or reset:
        out_file_valid = False
        already_done = pd.DataFrame().reindex(columns=dataframe.columns)
        start_line = 0

    elif isinstance(out_file, str):
        out_file_valid = True
        if os.path.isfile(out_file):
            already_done = pd.read_pickle(out_file)
            start_line = len(already_done)
        else:
            already_done = pd.DataFrame().reindex(columns=dataframe.columns)
            start_line = 0

    else:
        print('ERROR: "out_file" is of the wrong type, expected str')

    for i in tqdm(range(start_line, number_lines, chunksize)):
        sub_df = dataframe.iloc[i: i + chunksize]
        sub_df[out_column] = sub_df.apply(lambda x: zsl_multi_classifier(x, sequence, context, distrib), axis=1)
        already_done = already_done.append(sub_df)
        if out_file_valid:
            already_done.to_pickle(out_file)

    return already_done


def mean_pooling(model_output, attention_mask):
    token_embeddings = model_output[0]  # First element of model_output contains all token embeddings
    input_mask_expanded = attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
    return torch.sum(token_embeddings * input_mask_expanded, 1) / torch.clamp(input_mask_expanded.sum(1), min=1e-9)

def get_embeddings(model, text):
    # Tokenize sentences
    encoded_input = tokenizer(text, padding=True, truncation=True, return_tensors="pt")
    # Compute token embeddings
    with torch.no_grad():
        model_output = model(**encoded_input)
    # Perform pooling. In this case, max pooling.
    sentence_embeddings = mean_pooling(model_output, encoded_input["attention_mask"])
    return sentence_embeddings.cpu().squeeze(0)

def cosine_similarity(a, b):
    return dot(a, b)/(norm(a)*norm(b))


if __name__ == "__main__":

    # Retrieve arguments
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("task", choices=["A", "B"], type=str)
    argument_parser.add_argument("data_file", type=Path, action="store",
                                 help="The file with the ground truth labels")

    args = argument_parser.parse_args()

    # Get Data
    df = pd.read_csv(args.data_file)

    print("============== STEP 1: ENTAILMENT ==============")

    print("Load model from HuggingFace")

    device = "cuda:0" if torch.cuda.is_available() else "cpu"

    classifier = AutoModelForSequenceClassification.from_pretrained('facebook/bart-large-mnli').to(device)
    tokenizer = AutoTokenizer.from_pretrained('facebook/bart-large-mnli')

    print("Entailment from Premise to Conclusion (prob. distribution)")
    if args.task == 'A':
        df = run(df, None, 'EntailmentPtoC_distrib', 'Premise', 'Conclusion', distrib=True)

    elif args.task == 'B':
        df = run(df, None, 'EntailmentPtoC_distrib_1', 'Premise', 'Conclusion 1', distrib=True)
        df = run(df, None, 'EntailmentPtoC_distrib_2', 'Premise', 'Conclusion 2', distrib=True)

    print("Entailment from Conclusion to Premise (prob. distribution)")
    if args.task == 'A':
        df = run(df, None, 'EntailmentCtoP_distrib', 'Conclusion', 'Premise', distrib=True)

    elif args.task == 'B':
        df = run(df, None, 'EntailmentCtoP_distrib_1', 'Conclusion 1', 'Premise', distrib=True)
        df = run(df, None, 'EntailmentCtoP_distrib_2','Conclusion 2', 'Premise', distrib=True)

    print("============== STEP 2: COSINE SIMILARITY ==============")

    print("Load model from HuggingFace")
    tokenizer = AutoTokenizer.from_pretrained("usc-isi/sbert-roberta-large-anli-mnli-snli")
    model = AutoModel.from_pretrained("usc-isi/sbert-roberta-large-anli-mnli-snli")

    if args.task == 'A':
        print("Get SBERT embeddings")
        df['SBERT_premise'] = df['Premise'].progress_apply(lambda x: get_embeddings(model, x))
        df['SBERT_conclusion'] = df['Conclusion'].progress_apply(lambda x: get_embeddings(model, x))

        print("Measure cosine similarity")
        df['SBERT_cosine_sim'] = df.progress_apply(lambda x: cosine_similarity(x['SBERT_premise'], x['SBERT_conclusion']), axis=1)

    elif args.task == 'B':
        print("Get SBERT embeddings")
        df['SBERT_premise'] = df['Premise'].progress_apply(lambda x: get_embeddings(model, x))
        df['SBERT_conclusion_1'] = df['Conclusion 1'].progress_apply(lambda x: get_embeddings(model, x))
        df['SBERT_conclusion_2'] = df['Conclusion 2'].progress_apply(lambda x: get_embeddings(model, x))

        print("Measure cosine similarity")
        df['SBERT_cosine_sim_1'] = df.progress_apply(
            lambda x: cosine_similarity(x['SBERT_premise'], x['SBERT_conclusion_1']), axis=1)
        df['SBERT_cosine_sim_2'] = df.progress_apply(
            lambda x: cosine_similarity(x['SBERT_premise'], x['SBERT_conclusion_2']), axis=1)

    df.to_csv(str(args.data_file).replace('.csv', '_neural.csv'))
