# KEViN: A Knowledge Enhanced Validity and Novelty Classifier for Arguments

## Replicate our results


### Step 1: Generate features


#### 1.1. Neural features

```python 1a_generate_neural_features.py A ./data/TaskA_train.csv```

```python 1a_generate_neural_features.py A ./data/TaskA_dev.csv```

```python 1a_generate_neural_features.py A ./data/TaskA_test.csv```

```python 1a_generate_neural_features.py B ./data/TaskB_test.csv```

#### 1.2. KG features

```python 1b_kg_feature.py A ./data/TaskA_train.csv ./property/propertyCatagories.txt```

```python 1b_kg_feature.py A ./data/TaskA_dev.csv ./property/propertyCatagories.txt```

```python 1b_kg_feature.py A ./data/TaskA_test.csv ./property/propertyCatagories.txt```

```python 1b_kg_feature.py B ./data/TaskB_test.csv ./property/propertyCatagories.txt```


### Step 2: Pre-process the data

```python 2_preprocess.py ./data/TaskA_train.csv ./data/TaskA_dev.csv ./data/TaskA_test.csv ./data/TaskB_test.csv```


### Step 3: Perform a grid-search to find the best features and hyperparameters

```python 3_grid_search.py ./data/TaskA_train_normed.csv ./data/TaskA_dev_normed.csv```


**Note**: If there is any broken rows in the csv, which can be caused by opening/saving the csv file with Numbers/Excel, you will need to get rid of them. To perform the grid search for KEViN-2, which is composed of 2 models that predict validity and novelty separately, add the flag ```--indep``` at the end.


### Step 4: Get the KEViN predictions

* **KEViN-1 for Task A**: ```python 4_predict_kevin.py A ./data/TaskA_train_normed.csv ./data/TaskA_test_normed.csv ./data/best_features_joint.json```
* **KEViN-2 for Task A**: ```python 4_predict_kevin.py A ./data/TaskA_train_normed.csv ./data/TaskA_test_normed.csv ./data/best_features_indep.json```
* **KEViN-1 for Task B**: ```python 4_predict_kevin.py B ./data/TaskA_train_normed.csv ./data/TaskB_test_normed.csv ./data/best_features_joint.json```


### Step 5: Evaluate predictions against the ground truth

* **KEViN-1 for Task A**: ```python 5_evaluate.py A ./data/TaskA_test.csv ./data/TaskA_test_normed_preds_joint.csv```
* **KEViN-2 for Task A**: ```python 5_evaluate.py A ./data/TaskA_test.csv ./data/TaskA_test_normed_preds_indep.csv```
* **KEViN-1 for Task B**: ```python 5_evaluate.py B ./data/TaskB_test.csv ./data/TaskB_test_normed_preds_joint.csv```

**Note**: add the flag "--verbose" at the end of the command to see the fine-grained evaluation.

## Experiments

### Experiment 1: Compare to a RoBERTa baseline

* Train the RoBERTa baseline: ```python train_roberta.py ./data/TaskA_train_normed.csv ./data/TaskA_dev_normed.csv ./models/RoBERTa```
* Get the RoBERTa predictions: ```python predict_roberta.py ./data/TaskA_test_normed.csv ./models/RoBERTa```
* Evaluate the RoBERTa baseline: ```python 5_evaluate.py A ./data/TaskA_test.csv ./data/TaskA_test_normed_roberta.csv```

**Note**: by default, these scripts fine-tune a single model for both tasks. Add the flag ```--indep``` at the end of the `train_roberta` and `predict_roberta` commands to train two separate models. 
**Note 2**: this implementation of the baseline only works for Task A.

### Experiment 2: Perform the ablation study

```python ablation_study.py ./data/TaskA_train_normed.csv ./data/TaskA_test_normed.csv ./data/grid_search_joint.csv```
