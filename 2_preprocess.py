import os
import argparse
from pathlib import Path

import pandas as pd
from sklearn.preprocessing import MinMaxScaler

from utils import duplicate_ambiguous, upsample_training_data

random_state=102349

if __name__ == '__main__':

    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("train_file", type=Path, action="store",
                                 help="The file with the training data")
    argument_parser.add_argument("dev_file", type=Path, action="store",
                                 help="The file with the dev data")
    argument_parser.add_argument("test_file", type=Path, action="store",
                                 help="The file with the test data")
    argument_parser.add_argument("test_file_B", type=Path, action="store",
                                 help="The file with the test data")
    args = argument_parser.parse_args()

    df = pd.read_csv(str(args.train_file).replace('.csv', '_neural.csv'), index_col=0)
    df_kg = pd.read_csv(str(args.train_file).replace('.csv', '_kg.csv'), index_col=0)

    df_dev = pd.read_csv(str(args.dev_file).replace('.csv', '_neural.csv'), index_col=0)
    df_dev_kg = pd.read_csv(str(args.dev_file).replace('.csv', '_kg.csv'), index_col=0)

    df_test = pd.read_csv(str(args.test_file).replace('.csv', '_neural.csv'), index_col=0)
    df_test_kg = pd.read_csv(str(args.test_file).replace('.csv', '_kg.csv'), index_col=0)

    df_test_B = pd.read_csv(str(args.test_file_B).replace('.csv', '_neural.csv'), index_col=0)
    df_test_kg_B = pd.read_csv(str(args.test_file_B).replace('.csv', '_kg.csv'), index_col=0)

    ent_cols = df.columns[df.columns.str.contains('distrib')]

    df[ent_cols] = df[ent_cols].applymap(lambda x: eval(', '.join(x.split())))
    df_dev[ent_cols] = df_dev[ent_cols].applymap(lambda x: eval(', '.join(x.split())))
    df_test[ent_cols] = df_test[ent_cols].applymap(lambda x: eval(', '.join(x.split())))

    # Split df_test_B into 2 dataframes, one for each conclusion
    columns = df_test_B.columns

    to_drop = [c for c in columns if c[-2:] == '_2']
    df_test_1 = df_test_B.drop(to_drop, axis=1)
    new_column_names = {c: c.replace('_1', '') for c in columns if c[-2:] == '_1'}
    df_test_1 = df_test_1.rename(columns=new_column_names)

    to_drop = [c for c in columns if c[-2:] == '_1']
    df_test_2 = df_test_B.drop(to_drop, axis=1)
    new_column_names = {c: c.replace('_2', '') for c in columns if c[-2:] == '_2'}
    df_test_2 = df_test_2.rename(columns=new_column_names)

    # Split df_test_kg_B into 2 dataframes, one for each conclusion
    columns = df_test_kg_B.columns

    to_drop = [c for c in columns if c[-2:] == '_2']
    df_test_kg_1 = df_test_kg_B.drop(to_drop, axis=1)
    new_column_names = {c: c.replace('_1', '') for c in columns if c[-2:] == '_1'}
    df_test_kg_1 = df_test_kg_1.rename(columns=new_column_names)

    to_drop = [c for c in columns if c[-2:] == '_1']
    df_test_kg_2 = df_test_kg_B.drop(to_drop, axis=1)
    new_column_names = {c: c.replace('_2', '') for c in columns if c[-2:] == '_2'}
    df_test_kg_2 = df_test_kg_2.rename(columns=new_column_names)


    df = pd.merge(df, df_kg, on=list(set(df.columns) & set(df_kg.columns)))
    df_dev = pd.merge(df_dev, df_dev_kg, on=list(set(df_dev.columns) & set(df_dev_kg.columns)))
    df_test = pd.merge(df_test, df_test_kg, on=list(set(df_test.columns) & set(df_test_kg.columns)))

    df_test_1 = pd.merge(df_test_1, df_test_kg_1, on=list(set(df_test_1.columns) & set(df_test_kg_1.columns)))
    df_test_2 = pd.merge(df_test_2, df_test_kg_2, on=list(set(df_test_2.columns) & set(df_test_kg_2.columns)))

    results = {'features':[], 'model':[], 'regularisation':[], 'score':[], 'label':[]}

    print(df.groupby(['Novelty', 'Validity']).size())

    upsampled = upsample_training_data(df)
    df_unamb = duplicate_ambiguous(upsampled)

    normalized_features = ['SBERT_cosine_sim',
           'irrelevancy',
           'sumDis',
           'minDis',
           'maxDis',
           'aveDisA', 'aveDisANew', 'aveDisAmeer1', 'aveDisAmeer2']

    for f in normalized_features:
        if not f in df_test_1:
            df_test_1[f] = 0
        if not f in df_test_2:
            df_test_2[f] = 0

    scaler = MinMaxScaler()

    comb = pd.concat([df_unamb, df_dev, df_test, df_test_1, df_test_2], axis=0).fillna(0)
    comb[normalized_features] = scaler.fit(comb[normalized_features])

    df_unamb[normalized_features] = scaler.transform(df_unamb[normalized_features])
    df_dev[normalized_features] = scaler.transform(df_dev[normalized_features])
    df_test[normalized_features] = scaler.transform(df_test[normalized_features])

    df_test_1[normalized_features] = scaler.fit_transform(df_test_1[normalized_features])
    df_test_2[normalized_features] = scaler.fit_transform(df_test_2[normalized_features])

    df_unamb.to_csv(str(args.train_file).replace('.csv', '_normed.csv'))
    df_dev.to_csv(str(args.dev_file).replace('.csv', '_normed.csv'))
    df_test.to_csv(str(args.test_file).replace('.csv', '_normed.csv'))

    df_test_1.to_csv(str(args.test_file_B).replace('.csv', '_normed_1.csv'))
    df_test_2.to_csv(str(args.test_file_B).replace('.csv', '_normed_2.csv'))