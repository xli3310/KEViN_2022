import os
import argparse
from pathlib import Path

import torch
from torch.utils.data import DataLoader
import pandas as pd
from transformers import RobertaTokenizer, RobertaForSequenceClassification

from utils import create_RoBERTa_dataset, get_valnov_annot

if __name__ == '__main__':

    # Parse input arguments
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("test_file", type=Path, action="store",
                                 help="The file with the test data")
    argument_parser.add_argument("model_dir", type=Path, action="store",
                                 help="The directory where the model is saved")
    argument_parser.add_argument("--indep", action="store_true", default=False,
                                 help="Set this to train separate models for the validity and novelty tasks")
    args = argument_parser.parse_args()

    device = 'cuda' if torch.cuda.is_available() else 'cpu'

    # Get data
    df_test = pd.read_csv(args.test_file)

    # Remove ambiguous datapoints in test set (sanity check)
    df_test = df_test[df_test['Validity'] != 0]
    df_test = df_test[df_test['Novelty'] != 0]

    if args.indep:
        configs = [
            {'nb_classes': 2, 'label_column': 'Validity', 'model_name': 'model_val'},
            {'nb_classes': 2, 'label_column': 'Novelty', 'model_name': 'model_nov'}
        ]
    else:
        df_test['Val&Nov'] = df_test.apply(get_valnov_annot, axis=1)
        configs = [
            {'nb_classes': 4, 'label_column': 'Val&Nov', 'model_name': 'model_valnov'},
        ]

    for config in configs:
        # Load the RoBERTa tokenizer
        print('Loading RoBERTa tokenizer...')
        tokenizer = RobertaTokenizer.from_pretrained('roberta-base', do_lower_case=True)

        # Load RobertaForSequenceClassification
        print('Loading RoBERTa model...')
        model = RobertaForSequenceClassification.from_pretrained("roberta-base",
                                                                 num_labels=config['nb_classes'],
                                                                 output_attentions=False,
                                                                 output_hidden_states=False,
                                                                 return_dict=False
                                                                 )

        # Predict on test set
        print('Make predictions on test set')
        checkpoint = torch.load(os.path.join(args.model_dir, config['model_name'] + "_best.pt"))
        print("Epoch:", checkpoint['epoch'])
        model.load_state_dict(checkpoint['model_state'])

        testing_set = create_RoBERTa_dataset(df_test, tokenizer, label_column=config['label_column'])
        test_iterator = DataLoader(testing_set, batch_size=16, shuffle=False)

        model.eval()
        outputs = list()
        with torch.no_grad():
            for batch in test_iterator:
                b_input_ids = batch[0].to(device)
                b_input_mask = batch[1].to(device)
                b_labels = batch[2].to(device)
                outputs.append(model(b_input_ids,
                                     token_type_ids=None,
                                     attention_mask=b_input_mask,
                                     labels=b_labels)[1])

        outputs_flat = [e for l in outputs for e in l]
        target_column = config['label_column'] + '_preds'
        df_test[target_column] = [torch.argmax(e.cpu()).item() for e in outputs_flat]

        if config['label_column'] == 'Val&Nov':
            df_test['predicted validity'] = df_test[target_column].apply(lambda x: 1 if x >= 2 else -1)
            df_test['predicted novelty'] = df_test[target_column].apply(lambda x: 1 if (x == 1 or x == 3) else -1)
        else:
            df_test['predicted ' + config['label_column'].lower()] = df_test[target_column].apply(lambda x: -1 if x == 0 else 1)

    # Clean up df_test
    columns_to_keep = ['Premise', 'Conclusion', 'topic', 'predicted validity', 'predicted novelty']
    columns_to_drop = [c for c in df_test.columns if not c in columns_to_keep]
    df_test = df_test.drop(columns_to_drop)

    df_test.to_csv(args.test_file.replace('.csv', '_roberta.csv'))