from typing import Dict

import pandas as pd
import numpy as np
import torch
from torch.utils.data import TensorDataset

from sklearn.neural_network import MLPClassifier

random_state=102349

def duplicate_ambiguous(df):
    for l in ['Validity', 'Novelty']:
        sub=df[df[l] == 0]
        pos = sub.copy()
        pos[l] = 1
        neg = sub.copy()
        neg[l] = -1
        sub=df[df[l] == 0]
        pos = sub.copy()
        pos[l] = 1
        neg = sub.copy()
        neg[l] = -1
        df = pd.concat([df[~(df[l] == 0)], pos, neg], axis=0)
    df[['Novelty', 'Validity']] = (df[['Novelty', 'Validity']] > 0).astype(int)
    return df

def flatten_df(df):
    return np.array(list(map(np.concatenate, df.applymap(np.atleast_1d).to_numpy())))

def upsample_training_data(df):
    new_samples1 = df[(df['Novelty'] == 1) & (df['Validity'] == -1)].sample(250, replace=True, random_state=random_state)
    new_samples2 = df[(df['Novelty'] == 1) & (df['Validity'] == 1)].sample(200, replace=True, random_state=random_state)
    upsampled = pd.concat([df, new_samples1, new_samples2], axis=0)
    return upsampled

def get_valnov_annot(row):
    if row['Validity'] == -1:
        if row['Novelty'] == -1:
            return 0
        elif row['Novelty'] == 1:
            return 1
    elif row['Validity'] == 1:
        if row['Novelty'] == -1:
            return 2
        elif row['Novelty'] == 1:
            return 3


def create_RoBERTa_dataset(df, tokenizer, label_column):
    input_ids = []
    attention_masks = []
    labels = []

    for index, row in df.iterrows():
        dic = tokenizer.encode_plus(row['Premise'] + " [SEP] " + row['Conclusion'],     # Sentence to encode.
                                    add_special_tokens=True,                            # Add '[CLS]' and '[SEP]'
                                    max_length=300,                                     # Pad & truncate all sentences.
                                    padding='max_length',
                                    truncation=True,
                                    return_attention_mask=True,                         # Construct attn. masks.
                                    return_tensors='pt',                                # Return pytorch tensors.
                                    )
        input_ids.append(dic['input_ids'])
        attention_masks.append(dic['attention_mask'])
        labels.append(row[label_column])

    input_ids = torch.cat(input_ids, dim=0)
    attention_masks = torch.cat(attention_masks, dim=0)
    labels = torch.tensor(labels, dtype=torch.long)

    dataset = TensorDataset(input_ids, attention_masks, labels)

    return dataset


def val_nov_metric(is_validity: np.ndarray, should_validity: np.ndarray, is_novelty: np.ndarray, should_novelty: np.ndarray) -> Dict[str, float]:
    ret = dict()

    ret_base_help = {
        "true_positive_validity": np.sum(np.where(
            np.all(np.stack([is_validity >= .5, should_validity >= .5]), axis=0),
            1, 0)),
        "true_positive_novelty": np.sum(np.where(
            np.all(np.stack([is_novelty >= .5, should_novelty >= .5]), axis=0),
            1, 0)),
        "true_positive_valid_novel": np.sum(np.where(
            np.all(np.stack([is_validity >= .5, is_novelty >= .5,
                                   should_validity >= .5, should_novelty >= .5]), axis=0),
            1, 0)),
        "true_positive_nonvalid_novel": np.sum(np.where(
            np.all(np.stack([is_validity < .5, is_novelty >= .5,
                                   should_validity < .5, should_novelty >= .5]), axis=0),
            1, 0)),
        "true_positive_valid_nonnovel": np.sum(np.where(
            np.all(np.stack([is_validity >= .5, is_novelty < .5,
                                   should_validity >= .5, should_novelty < .5]), axis=0),
            1, 0)),
        "true_positive_nonvalid_nonnovel": np.sum(np.where(
            np.all(np.stack([is_validity < .5, is_novelty < .5,
                                   should_validity < .5, should_novelty < .5]), axis=0),
            1, 0)),
        "classified_positive_validity": np.sum(np.where(is_validity >= .5, 1, 0)),
        "classified_positive_novelty": np.sum(np.where(is_novelty >= .5, 1, 0)),
        "classified_positive_valid_novel": np.sum(np.where(
            np.all(np.stack([is_validity >= .5, is_novelty >= .5]), axis=0),
            1, 0)),
        "classified_positive_nonvalid_novel": np.sum(np.where(
            np.all(np.stack([is_validity < .5, is_novelty >= .5]), axis=0),
            1, 0)),
        "classified_positive_valid_nonnovel": np.sum(np.where(
            np.all(np.stack([is_validity >= .5, is_novelty < .5]), axis=0),
            1, 0)),
        "classified_positive_nonvalid_nonnovel": np.sum(np.where(
            np.all(np.stack([is_validity < .5, is_novelty < .5]), axis=0),
            1, 0)),
        "indeed_positive_validity": np.sum(np.where(should_validity >= .5, 1, 0)),
        "indeed_positive_novelty": np.sum(np.where(should_novelty >= .5, 1, 0)),
        "indeed_positive_valid_novel": np.sum(np.where(
            np.all(np.stack([should_validity >= .5, should_novelty >= .5]), axis=0),
            1, 0)),
        "indeed_positive_nonvalid_novel": np.sum(np.where(
            np.all(np.stack([should_validity < .5, should_novelty >= .5]), axis=0),
            1, 0)),
        "indeed_positive_valid_nonnovel": np.sum(np.where(
            np.all(np.stack([should_validity >= .5, should_novelty < .5]), axis=0),
            1, 0)),
        "indeed_positive_nonvalid_nonnovel": np.sum(np.where(
            np.all(np.stack([should_validity < .5, should_novelty < .5]), axis=0),
            1, 0)),
    }

    ret_help = {
        "precision_validity": ret_base_help["true_positive_validity"] /
                              max(1, ret_base_help["classified_positive_validity"]),
        "precision_novelty": ret_base_help["true_positive_novelty"] /
                             max(1, ret_base_help["classified_positive_novelty"]),
        "recall_validity": ret_base_help["true_positive_validity"] /
                           max(1, ret_base_help["indeed_positive_validity"]),
        "recall_novelty": ret_base_help["true_positive_novelty"] /
                          max(1, ret_base_help["indeed_positive_novelty"]),
        "precision_valid_novel": ret_base_help["true_positive_valid_novel"] /
                                 max(1, ret_base_help["classified_positive_valid_novel"]),
        "precision_valid_nonnovel": ret_base_help["true_positive_valid_nonnovel"] /
                                    max(1, ret_base_help["classified_positive_valid_nonnovel"]),
        "precision_nonvalid_novel": ret_base_help["true_positive_nonvalid_novel"] /
                                    max(1, ret_base_help["classified_positive_nonvalid_novel"]),
        "precision_nonvalid_nonnovel": ret_base_help["true_positive_nonvalid_nonnovel"] /
                                       max(1, ret_base_help["classified_positive_nonvalid_nonnovel"]),
        "recall_valid_novel": ret_base_help["true_positive_valid_novel"] /
                              max(1, ret_base_help["indeed_positive_valid_novel"]),
        "recall_valid_nonnovel": ret_base_help["true_positive_valid_nonnovel"] /
                                 max(1, ret_base_help["indeed_positive_valid_nonnovel"]),
        "recall_nonvalid_novel": ret_base_help["true_positive_nonvalid_novel"] /
                                 max(1, ret_base_help["indeed_positive_nonvalid_novel"]),
        "recall_nonvalid_nonnovel": ret_base_help["true_positive_nonvalid_nonnovel"] /
                                    max(1, ret_base_help["indeed_positive_nonvalid_nonnovel"])
    }

    ret.update({
        "f1_validity": 2 * ret_help["precision_validity"] * ret_help["recall_validity"] / max(1e-4, ret_help["precision_validity"] + ret_help["recall_validity"]),
        "f1_novelty": 2 * ret_help["precision_novelty"] * ret_help["recall_novelty"] / max(1e-4, ret_help["precision_novelty"] + ret_help["recall_novelty"]),
        "f1_valid_novel": 2 * ret_help["precision_valid_novel"] * ret_help["recall_valid_novel"] / max(1e-4, ret_help["precision_valid_novel"] + ret_help["recall_valid_novel"]),
        "f1_valid_nonnovel": 2 * ret_help["precision_valid_nonnovel"] * ret_help["recall_valid_nonnovel"] / max(1e-4, ret_help["precision_valid_nonnovel"] + ret_help["recall_valid_nonnovel"]),
        "f1_nonvalid_novel": 2 * ret_help["precision_nonvalid_novel"] * ret_help["recall_nonvalid_novel"] / max(1e-4, ret_help["precision_nonvalid_novel"] + ret_help["recall_nonvalid_novel"]),
        "f1_nonvalid_nonnovel": 2 * ret_help["precision_nonvalid_nonnovel"] * ret_help["recall_nonvalid_nonnovel"] / max(1e-4, ret_help["precision_nonvalid_nonnovel"] + ret_help["recall_nonvalid_nonnovel"])
    })

    ret.update({
        "f1_macro": (ret["f1_valid_novel"]+ret["f1_valid_nonnovel"]+ret["f1_nonvalid_novel"]+ret["f1_nonvalid_nonnovel"])/4
    })

    return ret



def train(train_df, features, target, reg_value,random_state=82):
    if target == 'ValNov':
        target = ['Validity', 'Novelty']
    else:
        target = [target]
    X_train = train_df[features]
    X_train = flatten_df(X_train)
    y_train = train_df[target]
    clf = MLPClassifier(solver='adam', hidden_layer_sizes=(5, 5), alpha=reg_value, random_state=random_state, max_iter=1000)
    clf.fit(X_train, y_train)
    return clf


def test(test_df, clf, features, target, fill_in_value=-1):
    X_test = test_df[features]
    X_test = flatten_df(X_test)
    test_pred = clf.predict(X_test)
    if target == 'ValNov':
        test_scores = val_nov_metric(test_pred[:, 0], test_df['Validity'], test_pred[:, 1], test_df['Novelty'])
    elif target == 'Validity':
        test_scores = val_nov_metric(test_pred, test_df['Validity'], np.full_like(test_df['Novelty'], fill_in_value), test_df['Novelty'])
    elif target == 'Novelty':
        test_scores = val_nov_metric(np.full_like(test_df['Validity'], fill_in_value), test_df['Validity'], test_pred, test_df['Novelty'])
    else:
        print("Target not recognised, should be one of ['ValNov', 'Validity', 'Novelty']")
    return test_scores