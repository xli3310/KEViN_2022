import os
import argparse
from pathlib import Path

import pandas as pd
from tqdm import tqdm

from utils import flatten_df, train

tqdm.pandas()
random_state=102349

def get_reg_value(df, features_list):
    sub_df = df[df['features'].apply(lambda x: sorted(x) == sorted(features_list))]
    max_x = sub_df.loc[sub_df['score'].idxmax()]
    return max_x['reg_value']


if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("train_file", type=Path, action="store",
                                 help="The file with the training data")
    argument_parser.add_argument("test_file", type=Path, action="store",
                                 help="The file with the test data")
    argument_parser.add_argument("grid_search_file", type=Path, action="store",
                                 help="The file with the test data")
    args = argument_parser.parse_args()

    df = pd.read_csv(args.train_file, index_col=0)
    df_test = pd.read_csv(args.test_file, index_col=0)

    config = pd.read_csv(args.grid_search_file)

    ent_cols = df.columns[df.columns.str.contains('distrib')]

    df[ent_cols] = df[ent_cols].applymap(eval)
    df_test[ent_cols] = df_test[ent_cols].applymap(eval)
    config['features'] = config['features'].apply(eval)

    DATA_PATH = os.path.dirname(args.test_file)

    features_lists = [
          ['EntailmentCtoP_distrib', 'irrelevancy', 'aveDisAmeer2'],
          ['EntailmentPtoC_distrib', 'irrelevancy', 'aveDisAmeer2'],
          ['EntailmentPtoC_distrib', 'EntailmentCtoP_distrib', 'aveDisAmeer2'],
          ['EntailmentPtoC_distrib', 'EntailmentCtoP_distrib', 'irrelevancy'],
          ['EntailmentPtoC_distrib', 'EntailmentCtoP_distrib'],
          ['irrelevancy', 'aveDisAmeer2']
      ]

    for i,f in enumerate(features_lists):

        reg_value = get_reg_value(config, f)

        # Train model
        clf = train(train_df=df, features=f, target='ValNov',
                    reg_value=reg_value, random_state=random_state)

        X_test = flatten_df(df_test[f])
        preds_test = clf.predict(X_test)
        df_test['predicted validity'] = preds_test[:, 0]
        df_test['predicted novelty'] = preds_test[:, 1]

        df_test['predicted validity'] = df_test['predicted validity'].apply(lambda x: -1 if x == 0 else x)
        df_test['predicted novelty'] = df_test['predicted novelty'].apply(lambda x: -1 if x == 0 else x)
        columns_to_keep = ['topic', 'Premise', 'Conclusion', 'predicted validity', 'predicted novelty']

        columns_to_drop = [c for c in df_test.columns if not c in columns_to_keep]
        df_test_clean = df_test.drop(columns_to_drop, axis=1)

        OUTPUT_FILE = 'ablation_test_joined_' + str(i) + '.csv'

        df_test_clean.to_csv(os.path.join(DATA_PATH, OUTPUT_FILE), encoding="utf-8")