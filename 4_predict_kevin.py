import json
import argparse
from pathlib import Path

import pandas as pd

from utils import flatten_df, train

random_state=102349


if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("task", choices=["A", "B"], type=str)
    argument_parser.add_argument("train_file", type=Path, action="store",
                                 help="The file with the training data")
    argument_parser.add_argument("test_file", type=Path, action="store",
                                 help="The file with the test data")
    argument_parser.add_argument("config_file", type=Path, action="store",
                                 help="The file with the test data")
    args = argument_parser.parse_args()

    out_file = str(args.test_file).replace('.csv', '_preds.csv')

    df = pd.read_csv(args.train_file, index_col=0)

    if args.task == 'A':
        df_test = pd.read_csv(args.test_file, index_col=0)
    else:
        df_test_1 = pd.read_csv(str(args.test_file).replace('.csv', '_1.csv'), index_col=0)
        df_test_2 = pd.read_csv(str(args.test_file).replace('.csv', '_2.csv'), index_col=0)

    with open(args.config_file) as file:
        config = json.load(file)

    ent_cols = df.columns[df.columns.str.contains('distrib')]
    df[ent_cols] = df[ent_cols].applymap(eval)
    targets = list(config.keys())

    # Train model
    clfs = {}
    for target in targets:
        clfs[target] = train(train_df=df, features=config[target]['features'], target=target,
                    reg_value=config[target]['reg_value'], random_state=random_state)

    print('Running on test set...')

    if args.task == 'A':
        df_test[ent_cols] = df_test[ent_cols].applymap(eval)
        if len(targets) > 1:
            for target in targets:
                X_test = flatten_df(df_test[config[target]['features']])
                preds_test = clfs[target].predict(X_test)
                column_name = 'predicted ' + target.lower()
                df_test[column_name] = preds_test
            OUTPUT_FILE = out_file.replace('.csv', '_indep.csv')

        else:
            X_test = flatten_df(df_test[config[targets[0]]['features']])
            preds_test = clfs[target].predict(X_test)
            df_test['predicted validity'] = preds_test[:, 0]
            df_test['predicted novelty'] = preds_test[:, 1]
            OUTPUT_FILE = out_file.replace('.csv', '_joint.csv')

        df_test['predicted validity'] = df_test['predicted validity'].apply(lambda x: -1 if x==0 else x)
        df_test['predicted novelty'] = df_test['predicted novelty'].apply(lambda x: -1 if x == 0 else x)
        columns_to_keep = ['topic', 'Premise', 'Conclusion', 'predicted validity', 'predicted novelty']


    elif args.task == 'B':
        df_test_1[ent_cols] = df_test_1[ent_cols].applymap(lambda x: eval(', '.join(x.split())))
        df_test_2[ent_cols] = df_test_2[ent_cols].applymap(lambda x: eval(', '.join(x.split())))

        X_test_1 = flatten_df(df_test_1[config[targets[0]]['features']])
        X_test_2 = flatten_df(df_test_2[config[targets[0]]['features']])
        preds_test_1 = clfs[target].predict_proba(X_test_1)
        preds_test_2 = clfs[target].predict_proba(X_test_2)
        df_test_1['predicted validity 1'] = preds_test_1[:, 0]
        df_test_2['predicted validity 2'] = preds_test_2[:, 0]
        df_test_1['predicted novelty 1'] = preds_test_1[:, 1]
        df_test_2['predicted novelty 2'] = preds_test_2[:, 1]
        OUTPUT_FILE = out_file.replace('.csv', '_joint.csv')

        df_test = pd.merge(df_test_1, df_test_2, on=['topic', 'Premise', 'Conclusion 1', 'Conclusion 2'])

        df_test['predicted validity'] = df_test.apply(
            lambda x: -1 if x['predicted validity 1'] > x['predicted validity 2'] else (
                1 if x['predicted validity 1'] < x['predicted validity 2'] else 0), axis=1)
        df_test['predicted novelty'] = df_test.apply(
            lambda x: -1 if x['predicted novelty 1'] > x['predicted novelty 2'] else (
                1 if x['predicted novelty 1'] < x['predicted novelty 2'] else 0), axis=1)

        columns_to_keep = ['topic', 'Premise', 'Conclusion 1', 'Conclusion 2', 'predicted validity', 'predicted novelty']

    columns_to_drop = [c for c in df_test.columns if not c in columns_to_keep]
    df_test_clean = df_test.drop(columns_to_drop, axis=1)
    df_test_clean.to_csv(OUTPUT_FILE, encoding="utf-8")