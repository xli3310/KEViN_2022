import os
import time
import argparse
from pathlib import Path

import pandas as pd
import torch
from torch import cuda
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt
from transformers import RobertaTokenizer, RobertaForSequenceClassification, AdamW

from utils import upsample_training_data, duplicate_ambiguous, get_valnov_annot, create_RoBERTa_dataset

random_state=102349


def epoch_time(start_time, end_time):
    '''Track training time. '''
    elapsed_time = end_time - start_time
    elapsed_mins = int(elapsed_time / 60)
    elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
    return elapsed_mins, elapsed_secs

def show_lossaccevol(tr_loss, val_loss):
    # Plot accuracy and loss
    fig, ax = plt.subplots(figsize=(7,5))
    ax.plot(val_loss, label='Validation loss')
    ax.plot(tr_loss, label='Training loss')
    ax.set_title('Losses')
    ax.set_xlabel('Epoch')
    ax.set_ylabel('Loss')
    ax.legend()
    plt.show()


def train(model, iterator, optimizer):
    '''Train the model with specified data, optimizer, and loss function. '''
    epoch_loss = 0
    model.train()

    for batch in iterator:
        b_input_ids = batch[0].to(device)
        b_input_mask = batch[1].to(device)
        b_labels = batch[2].to(device)

        # Reset the gradient to not use them in multiple passes
        optimizer.zero_grad()
        outputs = model(b_input_ids,
                        #  token_type_ids=None,
                        attention_mask=b_input_mask,
                        labels=b_labels)
        loss = outputs[0]

        # Backprop
        loss.backward()

        # Optimize the weights
        optimizer.step()

        # Record accuracy and loss
        epoch_loss += loss.item()

    return epoch_loss / len(iterator)


def evaluate(model, iterator):
    '''Evaluate model performance. '''
    epoch_loss = 0

    # Turm off dropout while evaluating
    model.eval()

    # No need to backprop in eval
    with torch.no_grad():
        for batch in iterator:
            b_input_ids = batch[0].to(device)
            b_input_mask = batch[1].to(device)
            b_labels = batch[2].to(device)

            outputs = model(b_input_ids,
                            #  token_type_ids=None,
                            attention_mask=b_input_mask,
                            labels=b_labels)
            loss = outputs[0]
            epoch_loss += loss.item()

    return epoch_loss / len(iterator)


def train_and_evaluate(model, optimizer, train_iterator, valid_iterator, path,
                       epochs=10, model_name='model'):
    best_valid_loss = float('inf')
    val_loss = []
    tr_loss = []

    for epoch in range(epochs):

        # Calculate training time
        start_time = time.time()

        # Get epoch losses and accuracies
        train_loss = train(model, train_iterator, optimizer)
        valid_loss = evaluate(model, valid_iterator)

        end_time = time.time()
        epoch_mins, epoch_secs = epoch_time(start_time, end_time)

        # Save training metrics
        val_loss.append(valid_loss)
        tr_loss.append(train_loss)

        # wandb.log({'accuracy': train_acc, 'loss': train_loss})

        if valid_loss < best_valid_loss:
            best_valid_loss = valid_loss

            # Save best epoch
            torch.save({'epoch': epoch,
                        'model_state': model.state_dict(),
                        'optimizer_state': optimizer.state_dict(),
                        'valid_loss': valid_loss}, path + "/" + model_name + "_best.pt")

        print(f'Epoch: {epoch + 1:2} | Epoch Time: {epoch_mins}m {epoch_secs}s')
        print(f'\tTrain Loss: {train_loss:.3f} | Val. Loss: {valid_loss:.3f}')

    torch.save({'epoch': epoch,
                'model_state': model.state_dict(),
                'optimizer_state': optimizer.state_dict(),
                'valid_loss': valid_loss}, path + "/" + model_name + "_last.pt")

    # Plot accuracy and loss
    show_lossaccevol(tr_loss, val_loss)

if __name__ == '__main__':

    # Parse input arguments
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("train_file", type=Path, action="store",
                                 help="The file with the training data")
    argument_parser.add_argument("dev_file", type=Path, action="store",
                                 help="The file with the dev data")
    argument_parser.add_argument("model_dir", type=Path, action="store",
                                 help="The directory where the target model will be saved")
    argument_parser.add_argument("--indep", action="store_true", default=False,
                                 help="Set this to train separate models for the validity and novelty tasks")
    args = argument_parser.parse_args()

    device = 'cuda' if cuda.is_available() else 'cpu'

    if not os.path.exists(args.model_dir):
        os.makedirs(args.model_dir)

    # Get data
    df_train = pd.read_csv(args.train_file)
    df_dev = pd.read_csv(args.dev_file)

    # Upsample training data and duplicate ambiguous datapoints
    upsampled = upsample_training_data(df_train)
    df_unamb = duplicate_ambiguous(upsampled)

    # Remove ambiguous datapoints in dev set
    df_dev = df_dev[df_dev['Validity'] != 0]
    df_dev = df_dev[df_dev['Novelty'] != 0]

    if args.indep:
        configs = [
            {'nb_classes': 2, 'label_column': 'Validity', 'model_name': 'model_val'},
            {'nb_classes': 2, 'label_column': 'Novelty', 'model_name': 'model_nov'}
        ]
    else:
        df_unamb['Val&Nov'] = df_unamb.apply(get_valnov_annot, axis=1)
        df_dev['Val&Nov'] = df_dev.apply(get_valnov_annot, axis=1)
        configs = [
            {'nb_classes': 4, 'label_column': 'Val&Nov', 'model_name': 'model_valnov'},
        ]

    for config in configs:

        print('====== Training', config['model_name'], '======')

        # Load the RoBERTa tokenizer
        print('Loading RoBERTa tokenizer...')
        tokenizer = RobertaTokenizer.from_pretrained('roberta-base', do_lower_case=True)

        # Load RobertaForSequenceClassification
        print('Loading RoBERTa model...')
        model = RobertaForSequenceClassification.from_pretrained("roberta-base",
                                                                 num_labels=config['nb_classes'],
                                                                 output_attentions=False,
                                                                 output_hidden_states=False,
                                                                 return_dict=False
                                                                 )
        model.to(device)

        # Batch data using a data iterator
        training_set = create_RoBERTa_dataset(df_unamb, tokenizer, label_column=config['label_column'])
        validation_set = create_RoBERTa_dataset(df_dev, tokenizer, label_column=config['label_column'])

        train_iterator = DataLoader(training_set, batch_size=16, shuffle=True)
        valid_iterator = DataLoader(validation_set, batch_size=16, shuffle=False)

        optimizer = AdamW(model.parameters(), lr=2e-5)

        # Fine-tune and validate RoBERTa model
        print('Fine-tuning RoBERTa...')
        train_and_evaluate(model, optimizer, train_iterator, valid_iterator, args.model_dir,
                           epochs=3, model_name=config['model_name'])